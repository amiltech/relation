//package com.amiltech.relation.controller;
//
//import com.amiltech.relation.entity.ShoppingCards;
//import com.amiltech.relation.service.ShoppingCardsService;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//
//import static org.mockito.ArgumentMatchers.anyInt;
//import static org.mockito.Mockito.when;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
////@WebMvcTest(controllers = ShoppingCardsController.class)
////@RunWith(SpringRunner.class)
//
//@SpringBootTest
//@AutoConfigureMockMvc
//public class ShoppingCardsControllerTest {
//
//    @Autowired
//    private MockMvc mockMvc;
//
//    @MockBean
//    ShoppingCardsService shoppingCardsService;
//
//
//    @Test
//    public void givenValidIdWhenGetShoppingCardThenSuccess() throws Exception {
//        //arrange
//        when(shoppingCardsService.findShoppingCardById(anyInt())).thenReturn(
//                ShoppingCards.builder()
//                        .id(1)
//                        .name("test")
//                        .build()
//        );
//
//        mockMvc.perform(get("/shopping-carts/1")).andExpect(status().isOk())
//                .andExpect(jsonPath("name").value("test"));
//    }
//
//    @Test
//    public void givenValidDataWhenGetShoppingCardThenSuccess() throws Exception {
//
//        ShoppingCards sp = ShoppingCards.builder()
//                .id(1)
//                .name("test")
//                .build();
//        //arrange
//        when(shoppingCardsService.findShoppingCardById(anyInt())).thenReturn(sp);
//
//        mockMvc.perform(post("/shopping-carts")
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(new ObjectMapper().writeValueAsString(sp)))
//                .andExpect(jsonPath("name").value("test"));
//    }
//
//}