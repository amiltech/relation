//package com.amiltech.relation.repository;
//
//import com.amiltech.relation.entity.ShoppingCards;
//import org.junit.Test;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.DisplayName;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
//import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
//
//import static org.junit.jupiter.api.Assertions.*;
//
//
//@DataJpaTest
////@Import({AppTestConfiguration.class})
//@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
//public class ShoppingCardsRepositoryTest {
//
//    @Autowired
//    private ShoppingCardsRepository shoppingCardsRepository;
//
//    private ShoppingCards shoppingCards;
//
//
//
//    @BeforeEach
//    public void setUp() {
//        shoppingCards = ShoppingCards.builder()
//                .name("Amil")
//                .build();
//        shoppingCardsRepository.save(shoppingCards);
//    }
//
//
//    @Test
//    @DisplayName("Given user when saved then can be found by id")
//    public void givenShppingCards_whenSaved_thenCanBeFoundById() {
//        var shoppingCards1 = shoppingCardsRepository.findById(shoppingCards.getId()).orElse(null);
//        assertNotNull(shoppingCards1);
//        assertEquals(shoppingCards1.getName(), shoppingCards.getName());
//    }
//
//    @Test
//    public void givenShoppingCardsThenNotFound() {
//        var shoppingCards1 = shoppingCardsRepository.findById(5);
//        assertFalse(shoppingCards1.isPresent());
//    }
//
//
//}
