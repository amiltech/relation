//package com.amiltech.relation;
//import com.amiltech.relation.dto.product.request.ProductCreateRequestDTO;
//import com.amiltech.relation.dto.shoppingCards.request.ShoppingCardAddProductRequestDTO;
//import com.amiltech.relation.dto.shoppingCards.request.ShoppingCardCreateRequestDTO;
//import com.amiltech.relation.entity.ShoppingCards;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.web.client.TestRestTemplate;
//import org.springframework.boot.test.web.server.LocalServerPort;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//
//import static org.assertj.core.api.Assertions.assertThat;
//
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//public class ApplicationE2ETest {
//
//    @LocalServerPort
//    private int port;
//
//    @Autowired
//    private TestRestTemplate restTemplate;
//
//    // Test creating a shopping cart
//    @Test
//    void testCreateShoppingCart() {
//        ShoppingCardCreateRequestDTO createRequest = new ShoppingCardCreateRequestDTO();
//        createRequest.setName("amil");
//
//        String url = "http://localhost:" + port + "/shopping-carts";
//
//        ResponseEntity<ShoppingCards> response = restTemplate.postForEntity(url, createRequest, ShoppingCards.class);
//
//        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
//        assertThat(response.getBody()).isNotNull();
//        assertThat(response.getBody().getId()).isNotNull();
//        assertThat(response.getBody().getName()).isEqualTo(createRequest.getName());
//    }
//
//    // Test adding a product to the cart
//    @Test
//    void testAddProductToCart() {
//        // Step 1: Create a shopping cart first
//        ShoppingCardCreateRequestDTO createRequest = new ShoppingCardCreateRequestDTO();
//        createRequest.setName("amil");
//        String urlCreate = "http://localhost:" + port + "/shopping-carts";
//        ShoppingCards shoppingCard = restTemplate.postForEntity(urlCreate, createRequest, ShoppingCards.class).getBody();
//
//        // Step 2: Add a product to the cart
//        ShoppingCardAddProductRequestDTO addProductRequest = new ShoppingCardAddProductRequestDTO();
//        addProductRequest.setProductId(1); // Assuming a product with ID 1 exists
//
//        String url = "http://localhost:" + port + "/shopping-carts/" + shoppingCard.getId() + "/product/add";
//
//        ResponseEntity<Void> response = restTemplate.postForEntity(url, addProductRequest, Void.class);
//
//        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
//    }
//
//    // Test removing a product from the cart
//    @Test
//    void testRemoveProductFromCart() {
//        // Step 1: Create a shopping cart first
//        ShoppingCardCreateRequestDTO createRequest = new ShoppingCardCreateRequestDTO();
//        createRequest.setName("amil");
//        String urlCreate = "http://localhost:" + port + "/shopping-carts";
//        ShoppingCards shoppingCard = restTemplate.postForEntity(urlCreate, createRequest, ShoppingCards.class).getBody();
//
//        // Step 2: Add a product to the cart first
//        ShoppingCardAddProductRequestDTO addProductRequest = new ShoppingCardAddProductRequestDTO();
//        addProductRequest.setProductId(1); // Assuming a product with ID 1 exists
//        String urlAdd = "http://localhost:" + port + "/shopping-carts/" + shoppingCard.getId() + "/product/add";
//        restTemplate.postForEntity(urlAdd, addProductRequest, Void.class);
//
//        // Step 3: Remove the product from the cart
//        String urlRemove = "http://localhost:" + port + "/shopping-carts/" + shoppingCard.getId() + "/product/remove";
//
//        ResponseEntity<Void> response = restTemplate.postForEntity(urlRemove, addProductRequest, Void.class);
//
//        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
//    }
//
//    // Test retrieving a shopping cart by ID
//    @Test
//    void testGetShoppingCartById() {
//        // Step 1: Create a shopping cart first
//        ShoppingCardCreateRequestDTO createRequest = new ShoppingCardCreateRequestDTO();
//        createRequest.setName("amil");
//        String urlCreate = "http://localhost:" + port + "/shopping-carts";
//        ShoppingCards shoppingCard = restTemplate.postForEntity(urlCreate, createRequest, ShoppingCards.class).getBody();
//
//        // Step 2: Retrieve the cart by ID
//        String url = "http://localhost:" + port + "/shopping-carts/" + shoppingCard.getId();
//
//        ResponseEntity<ShoppingCards> response = restTemplate.getForEntity(url, ShoppingCards.class);
//
//        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
//        assertThat(response.getBody()).isNotNull();
//        assertThat(response.getBody().getId()).isEqualTo(shoppingCard.getId());
//    }
//}
