//package com.amiltech.relation.service.impl;
//
//import static org.junit.jupiter.api.Assertions.*;
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.assertj.core.api.Assertions.assertThatThrownBy;
//import static org.junit.jupiter.api.Assertions.*;
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.ArgumentMatchers.anyLong;
//import static org.mockito.Mockito.times;
//import static org.mockito.Mockito.verify;
//import static org.mockito.Mockito.verifyNoInteractions;
//import static org.mockito.Mockito.when;
//
//import java.util.Optional;
//
//import com.amiltech.relation.dto.student.StudentDto;
//import com.amiltech.relation.entity.Student;
//import com.amiltech.relation.mapper.StudentMapper;
//import com.amiltech.relation.repository.StudentRepository;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.ArgumentCaptor;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.junit.jupiter.MockitoExtension;
//
//@ExtendWith(MockitoExtension.class)
//
//class StudentServiceTest {
//
//    @InjectMocks
//    StudentService studentService;
//    @Mock
//    StudentRepository studentRepository;
//    @Mock
//    StudentMapper studentMapper;
//
//    private Student student;
//    private StudentDto studentDto;
//
//    @BeforeEach
//    public void setUp() {
//        student = Student.builder()
//                .age(50)
//                .id(5L)
//                .name("Ali")
//                .active(true)
//                .password("102030")
//                .build();
//        studentDto = StudentDto.builder()
//                .age(50)
//                .id(5L)
//                .name("Ali")
//                .active(true)
//                .build();
//    }
//
//    @Test
//    void givenValidIdWhenGetStudentThenSuccess() {
//        //Arrange
//        when(studentRepository.findById(anyLong())).thenReturn(Optional.of(student));
//        when(studentMapper.studentToStudentDto(any())).thenReturn(studentDto);
//
//        //Act
//        StudentDto result = studentService.getStudent(5L);
//
//        //Assert
//        assertThat(result.getId()).isEqualTo(student.getId());
//        assertThat(result.getName()).isEqualTo(student.getName());
//        assertThat(result.getAge()).isEqualTo(student.getAge());
//        assertThat(result.getActive()).isEqualTo(student.getActive());
//        verify(studentRepository, times(1)).findById(anyLong());
//        verify(studentRepository, times(0)).save(any());
//    }
//
//    @Test
//    void givenInvalidIdWhenGetStudentThenException() {
//        //Arrange
//        when(studentRepository.findById(anyLong())).thenReturn(Optional.empty());
//
//        //Act & Assert
//        assertThatThrownBy(()-> studentService.getStudent(19L)).isInstanceOf(RuntimeException.class)
//                .hasMessage("Student with id 19 not found");
//        verify(studentMapper, times(0)).studentToStudentDto(any());
//    }
//
//
//    @Test
//    void givenValidIdWhenUpdateNameThenSuccess() {
//        //Arrange
//        Student student1 = new Student(student);
//        when(studentRepository.findById(anyLong())).thenReturn(Optional.of(student1));
//        when(studentRepository.save(any())).thenReturn(student1);
//
//        //Act
//        var result = studentService.updateName(5L);
//
//        // Assert
//        var captor = ArgumentCaptor.forClass(Student.class);
//        verify(studentRepository, times(1)).save(captor.capture());
//
//        Student saveMethodArgument = captor.getValue();
//        assertThat(saveMethodArgument.getId()).isEqualTo(student.getId());
//        assertThat(saveMethodArgument.getPassword()).isEqualTo(student.getPassword());
//        assertThat(saveMethodArgument.getActive()).isEqualTo(student.getActive());
//        assertThat(saveMethodArgument.getAge()).isEqualTo(student.getAge());
////        assertThat(saveMethodArgument.getName()).isEqualTo("amil");
//        assertThat(result.getName()).isEqualTo("amil");
//        assertThat(result.getPassword()).isEqualTo(student.getPassword());
//        assertThat(result.getActive()).isEqualTo(student.getActive());
//        assertThat(result.getAge()).isEqualTo(student.getAge());
//        verify(studentRepository, times(1)).findById(anyLong());
//
//    }
//}