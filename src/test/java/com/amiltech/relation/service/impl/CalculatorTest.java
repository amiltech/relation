//package com.amiltech.relation.service.impl;
//
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.junit.jupiter.params.ParameterizedTest;
//import org.junit.jupiter.params.provider.Arguments;
//import org.junit.jupiter.params.provider.CsvSource;
//import org.junit.jupiter.params.provider.MethodSource;
//import org.mockito.InjectMocks;
//import org.mockito.junit.jupiter.MockitoExtension;
//
//import java.util.stream.Stream;
//
//import static org.assertj.core.api.Assertions.assertThat;
//
//
//import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
//import static org.junit.jupiter.api.Assertions.*;
//
//@ExtendWith(MockitoExtension.class)
//class CalculatorTest {
//
//
//    @InjectMocks
//    private Calculator calculator;
//
//
//    @ParameterizedTest
//    @CsvSource({
//            "1, 2, 3",
//            "3, 5, 8",
//            "7, 8, 15"
//    })
//    void givenTwoNumbersWhenAddThenSuccess(int a, int b, int expectedResult) {
//        //Arrange
//
//        //Act
//        int actualResult = calculator.add(a, b);
//
//        //Assert
//        assertThat(actualResult).isEqualTo(expectedResult);
//    }
//
//    @ParameterizedTest
//    @MethodSource("valuesForDivide")
//    void givenTwoNumbersWhenDivideThenSuccess(int a, int b, int expectedResult) {
//        int result = calculator.divide(a, b);
//
//        assertThat(result).isEqualTo(expectedResult);
//    }
//
//    private static Stream<Arguments> valuesForDivide() {
//        return Stream.of(
//                Arguments.of("20", "5", "4"),
//                Arguments.of("30", "6", "5"),
//                Arguments.of("100", "1", "100")
//        );
//    }
//
//    @Test
//    void givenTwoNumbersWhenDivideToZeroThenException() {
//        //Arrange
//
//        //Act & Assert
//        assertThatThrownBy(() -> calculator.divide(20, 0))
//                .isInstanceOf(IllegalArgumentException.class)
//                .hasMessage("Division by zero is prohibited");
//    }
//
//}