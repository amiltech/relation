//package com.amiltech.relation.service.impl;
//
//import com.amiltech.relation.dto.shoppingCards.request.ShoppingCardAddProductRequestDTO;
//import com.amiltech.relation.dto.shoppingCards.request.ShoppingCardCreateRequestDTO;
//import com.amiltech.relation.entity.Product;
//import com.amiltech.relation.entity.ShoppingCards;
//import com.amiltech.relation.mapper.ShoppingCardMapper;
//import com.amiltech.relation.repository.ProductRepository;
//import com.amiltech.relation.repository.ShoppingCardsRepository;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.ArgumentCaptor;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Spy;
//import org.mockito.junit.jupiter.MockitoExtension;
//import org.springframework.data.redis.core.RedisTemplate;
//
//import jakarta.persistence.EntityNotFoundException;
//import org.springframework.data.redis.core.ValueOperations;
//
//import java.util.*;
//
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.assertj.core.api.Assertions.assertThatThrownBy;
//import static org.mockito.Mockito.*;
//
//@ExtendWith(MockitoExtension.class)
//class ShoppingCardsServiceImplWithRedisTemplateTest {
//
//    @InjectMocks
//    @Spy
//    private ShoppingCardsServiceImplWithRedisTemplate shoppingCardsService;
//
//    @Mock
//    private ShoppingCardsRepository shoppingCardsRepository;
//
//    @Mock
//    private ShoppingCardMapper shoppingCardMapper;
//
//    @Mock
//    private ProductRepository productRepository;
//
//    @Mock
//    private RedisTemplate<String, ShoppingCards> redisTemplate;
//
//
////    private Map<String, ShoppingCards> redisCache = new HashMap<>();
//
//    private ShoppingCards shoppingCard;
//    private ShoppingCardCreateRequestDTO createRequestDTO;
//    private ShoppingCardAddProductRequestDTO addProductRequestDTO;
//    private Product product;
//
//    @BeforeEach
//    void setUp() {
//        shoppingCard = ShoppingCards.builder()
//                .id(1)
//                .name("Test Shopping Card")
//                .products(new ArrayList<>())
//                .build();
//
//        createRequestDTO = new ShoppingCardCreateRequestDTO("Test Shopping Card");
//        addProductRequestDTO = new ShoppingCardAddProductRequestDTO(10);
//
//        product = Product.builder()
//                .id(10)
//                .active(true)
//                .build();
////
////
////        doAnswer(invocation -> {
////            String key = invocation.getArgument(0);
////            ShoppingCards value = invocation.getArgument(1);
////            redisCache.put(key, value); // Store data in the simulated cache
////            return null; // void method
////        }).when(valueOperations).set(anyString(), any(ShoppingCards.class));
//    }
//
//
//    @Test
//    void givenValidDTOWhenCreateShoppingCardThenSuccess() {
//        // Arrange
//        ShoppingCardCreateRequestDTO createRequestDTO = new ShoppingCardCreateRequestDTO("Test Card");
//
//        // Create an entity with null ID (before saving)
//        ShoppingCards shoppingCard = new ShoppingCards();
//        shoppingCard.setName("Test Card");
//
//        // Create an entity with generated ID (after saving)
//        ShoppingCards savedShoppingCard = new ShoppingCards();
//        savedShoppingCard.setId(1); // Simulate ID generation
//        savedShoppingCard.setName("Test Card");
//
//        ValueOperations<String, ShoppingCards> valueOperations = mock(ValueOperations.class);
//
//        // Mock dtoToEntity to return the entity without an ID
//        when(shoppingCardMapper.dtoToEntity(createRequestDTO)).thenReturn(shoppingCard);
//
//        // Mock save to return the entity with an ID
//        when(shoppingCardsRepository.save(shoppingCard)).thenReturn(savedShoppingCard);
//
//        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
//        doNothing().when(valueOperations).set(anyString(), any(ShoppingCards.class));
//
//
//        // Act
//        ShoppingCards result = shoppingCardsService.createShoppingCard(createRequestDTO);
//
//        // Assert
//        assertThat(result).isNotNull();
//        assertThat(result.getId()).isEqualTo(savedShoppingCard.getId()); // ID should match
//        assertThat(result.getName()).isEqualTo(savedShoppingCard.getName()); // Name should match
//
//        // Verify interactions
//        verify(shoppingCardMapper, times(1)).dtoToEntity(createRequestDTO);
//        verify(shoppingCardsRepository, times(1)).save(shoppingCard); // Verify save is called with entity having null ID
//        verify(valueOperations, times(1)).set("cardid::1", savedShoppingCard); // Verify caching
//    }
//    @Test
//    void findShoppingCardById_ShouldReturnCardFromCacheIfPresent() {
//        // Arrange
//        String redisKey = "cardid::1";
//        ValueOperations<String, ShoppingCards> valueOperations = mock(ValueOperations.class);
//        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
//        when(redisTemplate.opsForValue().get(redisKey)).thenReturn(shoppingCard);
//
//        // Act
//        ShoppingCards result = shoppingCardsService.findShoppingCardById(1);
//
//        // Assert
//        assertThat(result).isEqualTo(shoppingCard);
//        verify(shoppingCardsRepository, times(0)).findById(any());
//        verify(redisTemplate.opsForValue(), times(1)).get(redisKey);
//    }
//
//    @Test
//    void findShoppingCardById_ShouldLoadFromRepoIfNotInCache() {
//        // Arrange
//        String redisKey = "cardid::1";
//        ValueOperations<String, ShoppingCards> valueOperations = mock(ValueOperations.class);
//        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
//        when(redisTemplate.opsForValue().get(redisKey)).thenReturn(null); // Simulate cache miss
//        when(shoppingCardsRepository.findById(1)).thenReturn(Optional.of(shoppingCard));
//        doNothing().when(valueOperations).set(anyString(), any(ShoppingCards.class));
//
//        // Act
//        ShoppingCards result = shoppingCardsService.findShoppingCardById(1);
//
//        // Assert
//        assertThat(result).isEqualTo(shoppingCard);
////        verify(redisTemplate.opsForValue(), times(1)).get(redisKey);
//        verify(shoppingCardsRepository, times(1)).findById(1);
//        verify(redisTemplate.opsForValue(), times(1)).set(redisKey, shoppingCard);
//    }
//
//    @Test
//    void removeProductFromCart_ShouldRemoveProductFromShoppingCardAndUpdateCache() {
//        // Arrange
//        Integer shoppingCardId = 1;
//        Integer productId = 10;
//
//        // Create a shopping card with a product
//        ShoppingCardAddProductRequestDTO dto = new ShoppingCardAddProductRequestDTO(productId);
//
//        ValueOperations<String, ShoppingCards> valueOperations = mock(ValueOperations.class);
//
//
//        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
//        doReturn(shoppingCard).when(shoppingCardsService).findShoppingCardById(1);
////        doNothing().when(valueOperations).getAndDelete(anyString());
//        when(shoppingCardsRepository.save(shoppingCard)).thenReturn(shoppingCard);
//
//        // Act
//        shoppingCardsService.removeProductFromCart(shoppingCardId, dto);
//
//        // Assert
//        verify(shoppingCardsRepository, times(1)).save(shoppingCard);
//        verify(valueOperations,times(1)).getAndDelete(anyString());
//        verify(shoppingCardsService, times(1)).findShoppingCardById(shoppingCardId);
//    }
//
//    @Test
//    void addProductToCart_ShouldAddProductToShoppingCardAndSave() {
//        // Arrange
//        Integer shoppingCardId = 1;
//        Integer productId = 10;
//
//        // Create a shopping card and product
//        ShoppingCards shoppingCard = new ShoppingCards();
//        shoppingCard.setId(shoppingCardId);
//
//        Product product = new Product();
//        product.setId(productId);
//
//        ShoppingCardAddProductRequestDTO dto = new ShoppingCardAddProductRequestDTO(productId);
//
//        // Mock the repository methods
//        when(shoppingCardsService.findShoppingCardById(shoppingCardId)).thenReturn(shoppingCard);
//        when(productRepository.findById(productId)).thenReturn(Optional.of(product));
//        when(shoppingCardsRepository.save(shoppingCard)).thenReturn(shoppingCard);
//
//        // Act
//        ShoppingCards result = shoppingCardsService.addProductToCart(shoppingCardId, dto);
//
//        // Asser
//
//        // Verify interactions
//        verify(shoppingCardsService, times(1)).findShoppingCardById(shoppingCardId);
//        verify(productRepository, times(1)).findById(productId);
//    }
//
//}