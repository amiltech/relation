package com.amiltech.relation.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.Locale;

@Service
@RequiredArgsConstructor
public class TranslateService {
    @Autowired
    private MessageSource messageSource;

    public String translate(String code,String language,String... name) {
        return messageSource.getMessage(code, name,new Locale(language));
    }
}
