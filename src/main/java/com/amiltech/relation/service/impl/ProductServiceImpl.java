package com.amiltech.relation.service.impl;

import com.amiltech.relation.dto.product.request.ProductCreateRequestDTO;
import com.amiltech.relation.entity.Category;
import com.amiltech.relation.entity.Product;
import com.amiltech.relation.entity.ProductDetails;
import com.amiltech.relation.repository.CategoryRepository;
import com.amiltech.relation.repository.ProductDetailsRepository;
import com.amiltech.relation.repository.ProductRepository;
import com.amiltech.relation.service.ProductService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class ProductServiceImpl implements ProductService {
    CategoryRepository categoryRepository;
    ProductRepository productRepository;
    ProductDetailsRepository detailsRepository;

    @Override
    public void createProduct(ProductCreateRequestDTO dto) {
        ProductDetails productDetails = ProductDetails.builder()
                .image_url(dto.getProductDetails().getImage_url())
                .color(dto.getProductDetails().getColor())
                .build();


        Optional<Category> optionalCategory = categoryRepository.findById(dto.getCategoryId());
        if (optionalCategory.isPresent()) {
            Category category = optionalCategory.get();
            Product product = Product.builder()
                    .category(category)
                    .productDetails(productDetails)
                    .build();
            productRepository.save(product);
        } else {

            Category category = Category.builder()
                    .name("Default Category")
                    .build();

            Product product = Product.builder()
                    .productDetails(productDetails)
                    .category(category)
                    .build();
            productRepository.save(product); // Save Product
        }
    }
}
