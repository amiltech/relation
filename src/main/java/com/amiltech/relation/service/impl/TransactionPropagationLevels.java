package com.amiltech.relation.service.impl;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;

@Service
public class TransactionPropagationLevels {

    @Transactional(propagation = Propagation.REQUIRED) // Default propagation level
    public void method1() {
        method2(); // method2 will participate in method1's transaction if applicable
    }

    public void method2() {
        // Some logic here
    }
}
