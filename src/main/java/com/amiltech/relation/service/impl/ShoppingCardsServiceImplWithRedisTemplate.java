package com.amiltech.relation.service.impl;

import com.amiltech.relation.dto.shoppingCards.request.ShoppingCardAddProductRequestDTO;
import com.amiltech.relation.dto.shoppingCards.request.ShoppingCardCreateRequestDTO;
import com.amiltech.relation.entity.Product;
import com.amiltech.relation.entity.ShoppingCards;
import com.amiltech.relation.mapper.ShoppingCardMapper;
import com.amiltech.relation.repository.ProductRepository;
import com.amiltech.relation.repository.ShoppingCardsRepository;
import com.amiltech.relation.service.ShoppingCardsService;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.context.annotation.Primary;
//import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Primary
public class ShoppingCardsServiceImplWithRedisTemplate implements ShoppingCardsService {

    ShoppingCardsRepository shoppingCardsRepository;
    ShoppingCardMapper shoppingCardMapper;
    ProductRepository productRepository;
//    RedisTemplate<String, ShoppingCards> template;

    private static final String REDIS_KEY_PREFIX = "cardid::";

    @Override
    public ShoppingCards createShoppingCard(ShoppingCardCreateRequestDTO dto) {
        ShoppingCards cards = shoppingCardsRepository.save(shoppingCardMapper.dtoToEntity(dto));
//        template.opsForValue().set(REDIS_KEY_PREFIX + cards.getId(), cards);
        return cards;
    }

    @Override
    public ShoppingCards addProductToCart(Integer shoppingCardId, ShoppingCardAddProductRequestDTO dto) {
        ShoppingCards cards = findShoppingCardById(shoppingCardId);
        Product product = productRepository.findById(dto.getProductId())
                .orElseThrow(() -> new EntityNotFoundException("Product not found with ID: " + dto.getProductId()));
        if (cards.getProducts() == null) {
            cards.setProducts(new ArrayList<>());
        }
        cards.getProducts().add(product);

        // Save the updated card to the database and cache
        shoppingCardsRepository.save(cards);
//        template.opsForValue().set(REDIS_KEY_PREFIX + cards.getId(), cards);
        return cards;
    }

    @Override
//    @Transactional
    public void removeProductFromCart(Integer shoppingCardId, ShoppingCardAddProductRequestDTO dto) {
        ShoppingCards shoppingCard = findShoppingCardById(shoppingCardId);
        shoppingCard.getProducts().removeIf(product -> product.getId().equals(dto.getProductId()));

        // Save the updated card and remove the cache entry
//        template.opsForValue().getAndDelete(REDIS_KEY_PREFIX + shoppingCardId);
        shoppingCardsRepository.save(shoppingCard);
    }

    @Override
    public ShoppingCards findShoppingCardById(Integer shoppingCardId) {
        // Try to get the shopping card from the Redis cache
        String redisKey = REDIS_KEY_PREFIX + shoppingCardId;
//        ShoppingCards shoppingCards = template.opsForValue().get(redisKey);

        // If not found in cache, load from the database and store it in the cache
//        if (shoppingCards == null) {
//            shoppingCards = shoppingCardsRepository.findById(shoppingCardId)
//                    .orElseThrow(() -> new RuntimeException("Card not found with ID: " + shoppingCardId));
//            template.opsForValue().set(redisKey, shoppingCards);
//        }

//        return shoppingCards;
        return null;
    }

}
