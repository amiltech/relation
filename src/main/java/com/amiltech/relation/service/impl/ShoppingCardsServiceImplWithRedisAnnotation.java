package com.amiltech.relation.service.impl;

import com.amiltech.relation.dto.shoppingCards.request.ShoppingCardAddProductRequestDTO;
import com.amiltech.relation.dto.shoppingCards.request.ShoppingCardCreateRequestDTO;
import com.amiltech.relation.entity.Product;
import com.amiltech.relation.entity.ShoppingCards;
import com.amiltech.relation.mapper.ShoppingCardMapper;
import com.amiltech.relation.repository.ProductRepository;
import com.amiltech.relation.repository.ShoppingCardsRepository;
import com.amiltech.relation.service.ShoppingCardsService;
import jakarta.persistence.EntityNotFoundException;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class ShoppingCardsServiceImplWithRedisAnnotation implements ShoppingCardsService {

    ShoppingCardsRepository shoppingCardsRepository;
    ShoppingCardMapper shoppingCardMapper;
    ProductRepository productRepository;

    @Override
//    @CachePut(cacheNames = "card", key = "#result.id")
    public ShoppingCards createShoppingCard(ShoppingCardCreateRequestDTO dto) {
        ShoppingCards cards = shoppingCardsRepository.save(shoppingCardMapper.dtoToEntity(dto));
        return cards;
    }

    @Override
//    @CachePut(cacheNames = "card", key = "#shoppingCardId")
    public ShoppingCards addProductToCart(Integer shoppingCardId, ShoppingCardAddProductRequestDTO dto) {
        ShoppingCards cards = findShoppingCardById(shoppingCardId);
        Product product = productRepository.findById(dto.getProductId())
                .orElseThrow(() -> new EntityNotFoundException("Product not found with ID: " + dto.getProductId()));
        cards.getProducts().add(product);
        shoppingCardsRepository.save(cards);
        return cards;
    }


    @Override
//    @CacheEvict(cacheNames = "card", key = "#shoppingCardId")
    public void removeProductFromCart(Integer shoppingCardId, ShoppingCardAddProductRequestDTO dto) {
        ShoppingCards shoppingCard = findShoppingCardById(shoppingCardId);
        shoppingCard.getProducts().removeIf(product -> product.getId().equals(dto.getProductId()));
        shoppingCardsRepository.save(shoppingCard);


    }

    //    @Cacheable(cacheNames = "card", key = "#shoppingCardId")
    @Override
    public ShoppingCards findShoppingCardById(Integer shoppingCardId) {

        ShoppingCards shoppingCards = shoppingCardsRepository.findById(shoppingCardId).orElseThrow(() -> new RuntimeException("Card not found"));
        return shoppingCards;
    }
}
