package com.amiltech.relation.service.impl;


import com.amiltech.relation.dto.student.StudentDto;
import com.amiltech.relation.entity.Student;
import com.amiltech.relation.exception.ErrorCode;
import com.amiltech.relation.exception.StudentNotFoundException;
import com.amiltech.relation.mapper.StudentMapper;
import com.amiltech.relation.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.rmi.StubNotFoundException;

@Service
@RequiredArgsConstructor
@Slf4j
public class StudentService {

    private final StudentRepository studentRepository;
    private final StudentMapper studentMapper;

    public Student updateName(Long id) {
        Student student = studentRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Student not found"));
        student.setName("amil");
        studentRepository.save(student);
        return student;
    }



    public StudentDto getStudent(Long id) {
        log.info("Get from DB {}", id);
        Student student = studentRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format("Student with id %s not found", id)));
        StudentDto studentDto = studentMapper.studentToStudentDto(student);
//        studentDto.setName("a");
        return studentDto;
    }

    public void delete(Long id) {
        studentRepository.deleteById(id);
    }

    public Student create(Student student) {
        Student save = studentRepository.save(student);
        return save;
    }



    public void test(String[] test){
        throw new StudentNotFoundException(ErrorCode.STUDENT_NOT_FOUND_0001,test);
    }




}
