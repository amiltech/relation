package com.amiltech.relation.service.impl;

import com.amiltech.relation.client.Ms02FeignClient;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@RequiredArgsConstructor
public class TestService {

    private final RestTemplate restTemplate;
    private final Ms02FeignClient feignClient;

    public ResponseEntity<String> sayHello(){
        ResponseEntity<String> forEntity = restTemplate.getForEntity("http://localhost:9009/hello/ms-02", String.class);
        return forEntity;
    }

    public ResponseEntity<String> sayHello2(){
        String forEntity = feignClient.helloMs02();
        return ResponseEntity.ok(forEntity);
    }
}
