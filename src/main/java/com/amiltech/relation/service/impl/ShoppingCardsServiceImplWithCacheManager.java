//package com.amiltech.relation.service.impl;
//
//import com.amiltech.relation.dto.shoppingCards.request.ShoppingCardAddProductRequestDTO;
//import com.amiltech.relation.dto.shoppingCards.request.ShoppingCardCreateRequestDTO;
//import com.amiltech.relation.entity.ShoppingCards;
//import com.amiltech.relation.mapper.ShoppingCardMapper;
//import com.amiltech.relation.repository.ProductRepository;
//import com.amiltech.relation.repository.ShoppingCardsRepository;
//import com.amiltech.relation.service.ShoppingCardsService;
//import lombok.AccessLevel;
//import lombok.RequiredArgsConstructor;
//import lombok.experimental.FieldDefaults;
//import org.springframework.cache.Cache;
//import org.springframework.cache.CacheManager;
//import org.springframework.context.annotation.Primary;
//import org.springframework.stereotype.Service;
//
//import java.util.ArrayList;
//
//@Service
//@RequiredArgsConstructor
//@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
////@Primary
//public class ShoppingCardsServiceImplWithCacheManager implements ShoppingCardsService {
//
//    ShoppingCardsRepository shoppingCardsRepository;
//    ShoppingCardMapper shoppingCardMapper;
//    ProductRepository productRepository;
//    CacheManager cacheManager;
//
//
//
//    @Override
//    public ShoppingCards createShoppingCard(ShoppingCardCreateRequestDTO dto) {
//        ShoppingCards createdShoppingCard = shoppingCardsRepository.save(shoppingCardMapper.dtoToEntity(dto));
//        Cache cache = cacheManager.getCache("card");
//        cache.put(createdShoppingCard.getId(),createdShoppingCard);
//        return createdShoppingCard;
//    }
//
//    @Override
//    public ShoppingCards addProductToCart(Integer shoppingCardId, ShoppingCardAddProductRequestDTO dto) {
//        ShoppingCards shoppingCard = findShoppingCardById(shoppingCardId);
//        if (shoppingCard.getProducts() == null) {
//            shoppingCard.setProducts(new ArrayList<>());
//        }
//        shoppingCard.getProducts().add(productRepository.findById(dto.getProductId()).orElseThrow());
//        shoppingCardsRepository.save(shoppingCard);
//        Cache card = cacheManager.getCache("card");
//        card.put(shoppingCard.getId(),shoppingCard);
//        return shoppingCard;
//    }
//
//    @Override
//    public void removeProductFromCart(Integer shoppingCardId, ShoppingCardAddProductRequestDTO dto) {
//        ShoppingCards shoppingCard = findShoppingCardById(shoppingCardId);
//        shoppingCard.getProducts().removeIf(product -> product.getId().equals(dto.getProductId()));
//        shoppingCardsRepository.save(shoppingCard);
//        Cache card = cacheManager.getCache("card");
//        card.evict(shoppingCard.getId());
//
//    }
//
//    @Override
//    public ShoppingCards findShoppingCardById(Integer shoppingCardId) {
//        Cache card = cacheManager.getCache("card");
//        ShoppingCards shoppingCards = card.get(shoppingCardId, ShoppingCards.class);
//        if(shoppingCards == null){
//             shoppingCards = shoppingCardsRepository.findById(shoppingCardId).orElseThrow(() -> new RuntimeException("Card not found"));
//             card.put(shoppingCards.getId(),shoppingCards);
//        }
//        return shoppingCards;
//    }
//}
