package com.amiltech.relation.service.impl;


import com.amiltech.relation.entity.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.connection.stream.ObjectRecord;
import org.springframework.data.redis.connection.stream.StreamRecords;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.data.redis.connection.stream.RecordId;

import java.util.Objects;

@Service
@Slf4j
@RequiredArgsConstructor
public class EventPublisher {

    private final RedisTemplate<String, String> redisTemplate;

    @Value("lesson1-user-event")
    private String streamKey;

    public RecordId publishCustomerEvent(User user) {

        ObjectRecord<String, User> record = StreamRecords.newRecord()
                .ofObject(user)
                .withStreamKey(streamKey);
        RecordId recordId = redisTemplate.opsForStream().add(record);
        if (Objects.isNull(recordId)) {
            log.error("Error sending customer event: {}", user);
            return null;
        }
        return recordId;
    }
}
