package com.amiltech.relation.service.impl;

import com.amiltech.relation.config.BaseJwtService;
import com.amiltech.relation.dto.UserDto;
import com.amiltech.relation.entity.Authority;
import com.amiltech.relation.entity.User;
import com.amiltech.relation.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.amiltech.relation.constraints.Role.USER;

@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder passwordEncoder;
    private final BaseJwtService baseJwtService;
    private final EventPublisher eventPublisher;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return null;
    }

    public void createUser(UserDto userDto) {
        userRepository.findByUsername(userDto.getUsername()).ifPresentOrElse(
                user -> {
                    throw new IllegalArgumentException("User already exists");
                },
                () -> {
                    User user = User.builder()
                            .username(userDto.getUsername())
                            .password(passwordEncoder.encode(userDto.getPassword()))
                            .authorities(List.of(Authority.builder()
                                    .authority(USER)
                                    .build()))
                            .build();
                    userRepository.save(user);
                    eventPublisher.publishCustomerEvent(user);
                });
    }


    public String login(UserDto userDto) {
        User user = userRepository.findByUsername(userDto.getUsername())
                .orElseThrow(() -> new RuntimeException());
        boolean matches = passwordEncoder.matches(userDto.getPassword(), user.getPassword());
        if (!matches) {
            throw new IllegalArgumentException("Password is incorrect");
        } else {
            return baseJwtService.create(user);
        }
    }

}
