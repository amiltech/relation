package com.amiltech.relation.service.impl;

public class Calculator {
    public int add(int a, int b) {
        return a + b;
    }

    public int divide(int a, int b) {
        if (b == 0) {
            throw new IllegalArgumentException("Division by zero is prohibited");
        }
        return a / b;
    }
}
