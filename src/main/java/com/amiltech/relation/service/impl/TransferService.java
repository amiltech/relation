package com.amiltech.relation.service.impl;

import com.amiltech.relation.entity.Account;
import com.amiltech.relation.repository.AccountRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import java.io.IOException;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;



@Service
@RequiredArgsConstructor
@Slf4j
public class TransferService {

    private final AccountRepository accountRepository;
    private final EntityManagerFactory emf;

    //ACID
    public void transferSafe(long fromId, long toId, double amount) {
        EntityManager entityManager = emf.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            Account fromAccount = entityManager.createQuery("select a from Account a where a.id = :id", Account.class)
                    .setParameter("id", fromId)
                    .getSingleResult();
            if (fromAccount.getBalance() < amount) {
                throw new RuntimeException("Not enough balance");
            }
            Account toAccount = entityManager.createQuery("select a from Account a where a.id = :id", Account.class)
                    .setParameter("id", toId)
                    .getSingleResult();
            fromAccount.setBalance(fromAccount.getBalance() - amount);
            toAccount.setBalance(toAccount.getBalance() + amount);

            entityManager.persist(fromAccount);
            if (true) {
                throw new RuntimeException();
            }
            entityManager.persist(toAccount);
            entityManager.getTransaction().commit();
        } catch (RuntimeException e) {
            e.printStackTrace();
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }

    }

    @Transactional(rollbackFor = {IOException.class, Exception.class})
    @SneakyThrows
    public void transfer2(long from, long to, double amount) {
        Account fromAccount = accountRepository.findById(from)
                .orElseThrow(() -> new RuntimeException("Account not found"));
        if (fromAccount.getBalance() < amount) {
            throw new RuntimeException("Not enough balance");
        }

        Account toAccount = accountRepository.findById(to)
                .orElseThrow(() -> new RuntimeException("Account not found"));

        Thread.sleep(10000);



        fromAccount.setBalance(fromAccount.getBalance() - amount);
//        if (true) {
//            throw new IOException();
//        }
        toAccount.setBalance(toAccount.getBalance() + amount);;

    }
    @Transactional
    @SneakyThrows
    public void transfer(long from, long to, double amount){
        log.info("Thread {} -Getting Account with Id {} ",Thread.currentThread().getName(),from);
        Account fromAccount = accountRepository.findById(from)
                .orElseThrow(() -> new RuntimeException("Account not found"));
        log.info("Thread {} -Getting Account with Id {} is {}",Thread.currentThread().getName(),from,fromAccount);

        if (fromAccount.getBalance() < amount) {
            throw new RuntimeException("Not enough balance");
        }
        log.info("sleeping...");
        Thread.sleep(10000);

        log.info("Thread {} -Getting Account with Id {} ",Thread.currentThread().getName(),to);
        Account toAccount = accountRepository.findById(to)
                .orElseThrow(() -> new RuntimeException("Account not found"));

        log.info("Thread {} -Getting Account with Id {} is {}",Thread.currentThread().getName(),to,toAccount);



        log.info("Thread {} -changing balance of account {}",Thread.currentThread().getName(),from);
        fromAccount.setBalance(fromAccount.getBalance() - amount);
        log.info("Thread {} -changing balance of account {}",Thread.currentThread().getName(),to);
        toAccount.setBalance(toAccount.getBalance() + amount);;
    }
}

