package com.amiltech.relation.service.impl;

import com.amiltech.relation.dto.shoppingCards.request.ShoppingCardAddProductRequestDTO;
import com.amiltech.relation.dto.shoppingCards.request.ShoppingCardCreateRequestDTO;
import com.amiltech.relation.entity.Product;
import com.amiltech.relation.entity.ShoppingCards;
import com.amiltech.relation.mapper.ShoppingCardMapper;
import com.amiltech.relation.repository.ProductRepository;
import com.amiltech.relation.repository.ShoppingCardsRepository;
import com.amiltech.relation.service.ShoppingCardsService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class ShoppingCardsServiceImpl implements ShoppingCardsService {

    ShoppingCardsRepository shoppingCardsRepository;
    ShoppingCardMapper shoppingCardMapper;

    ProductRepository productRepository;

    @Override
    public ShoppingCards createShoppingCard(ShoppingCardCreateRequestDTO dto) {
        ShoppingCards createdShoppingCard = shoppingCardsRepository.save(shoppingCardMapper.dtoToEntity(dto));
        return createdShoppingCard;
    }

    @Override
    public ShoppingCards addProductToCart(Integer shoppingCardId, ShoppingCardAddProductRequestDTO dto) {
        ShoppingCards shoppingCard = shoppingCardsRepository.findById(shoppingCardId).orElseThrow(() -> new RuntimeException("Card not found"));
        shoppingCard.getProducts().add(productRepository.findById(dto.getProductId()).orElseThrow());
        shoppingCardsRepository.save(shoppingCard);
        return shoppingCard;
    }

    @Override
    public void removeProductFromCart(Integer shoppingCardId, ShoppingCardAddProductRequestDTO dto) {
        ShoppingCards shoppingCard = shoppingCardsRepository.findById(shoppingCardId).orElseThrow(() -> new RuntimeException("Card not found"));
        shoppingCard.getProducts().removeIf(product -> product.getId().equals(dto.getProductId()));
        shoppingCardsRepository.save(shoppingCard);
    }

    @Override
    public ShoppingCards findShoppingCardById(Integer shoppingCardId) {
        ShoppingCards shoppingCard = shoppingCardsRepository.findById(shoppingCardId).orElseThrow(() -> new RuntimeException("Card not found"));
        return shoppingCard
        ;
    }
}
