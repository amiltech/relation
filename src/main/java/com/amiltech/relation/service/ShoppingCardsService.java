package com.amiltech.relation.service;

import com.amiltech.relation.dto.shoppingCards.request.ShoppingCardAddProductRequestDTO;
import com.amiltech.relation.dto.shoppingCards.request.ShoppingCardCreateRequestDTO;
import com.amiltech.relation.entity.ShoppingCards;

public interface ShoppingCardsService {
    ShoppingCards createShoppingCard(ShoppingCardCreateRequestDTO dto);
    ShoppingCards addProductToCart (Integer shoppingCardId, ShoppingCardAddProductRequestDTO dto);
    void removeProductFromCart (Integer shoppingCardId, ShoppingCardAddProductRequestDTO dto);
    ShoppingCards findShoppingCardById(Integer shoppingCardId);

}
