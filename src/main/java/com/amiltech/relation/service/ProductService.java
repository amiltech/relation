package com.amiltech.relation.service;

import com.amiltech.relation.dto.product.request.ProductCreateRequestDTO;

public interface ProductService {

    void createProduct(ProductCreateRequestDTO dto);
}
