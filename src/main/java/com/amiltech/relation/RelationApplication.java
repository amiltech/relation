package com.amiltech.relation;

import com.amiltech.relation.entity.Account;
import com.amiltech.relation.repository.AccountRepository;
import com.amiltech.relation.repository.ProductRepository;
import com.amiltech.relation.service.impl.TransferService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;


@SpringBootApplication
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
//@Transactional
@EnableCaching
@EnableFeignClients
public class RelationApplication implements CommandLineRunner {

    ProductRepository productRepository;
    TransferService transferService;
    EntityManagerFactory entityManagerFactory;
    AccountRepository accountRepository;


    @Override
//    @Transactional
    public void run(String... args) throws Exception {

//        Account account = accountRepository.findById(3L).orElseThrow();
//        account.setBalance((double)(10-2));

////

////
//        accountRepository.save(account);
//        account.setBalance(1.0);
//        Account account = Account.builder()
//                .balance((double) 555)
//                .build();
////
//
//        EntityManager entityManager = entityManagerFactory.createEntityManager();
////////        Account.builder()
//        entityManager.getTransaction().begin();
//        entityManager.persist(account);
//        account.setBalance(123.0);
////        entityManager.flush();
//
//        entityManager.detach(account);
////
//        account.setBalance(0.0);
//
//        entityManager.merge(account);
//
//
//        account.setBalance(10.0);


//        entityManager.flush();


//        entityManager.close();
//
//
////
//
//        entityManager.getTransaction().commit();


//        EntityManager entityManager = entityManagerFactory.createEntityManager();
//
//        entityManager.getTransaction().begin();
//        Account account = Account.builder()
//                .balance((double) 555)
//                .build();
//
//        Account account2 = Account.builder()
//                .balance((double) 333)
//                .build();
//
//
//        entityManager.persist(account);
//        entityManager.persist(account2);
//
//
//        account.setBalance(1.1);
//        entityManager.flush();
//
//        account2.setBalance(1.2);
//
//        entityManager.detach(account);
//        entityManager.detach(account2);
//
//
//        entityManager.getTransaction().commit();
//
//        System.out.println("account1: " + accountRepository.findById(account.getId()).get().getBalance());
//        System.out.println("account2: " + accountRepository.findById(account2.getId()).get().getBalance());


//        Account account = entityManager
//                .createQuery("select a from Account as a where a.id =: id", Account.class)
//                .setParameter("id", 1)
//                .getSingleResult();
//
//        account.setBalance(345.0);
//
//
//        entityManager.getTransaction().commit();


//
//        Specification<Product> p = (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("active"), true);
//        Specification<Product> p2 = (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("active"), true);
//        System.out.println(productRepository.findAll(p.and(p2)));
//
//        transferService.transfer(1,2,50);
//
//
//        Specification<Product> p3 = (root, query, criteriaBuilder) -> {
//            return criteriaBuilder.equal(root.get("active"), true);
//        };


    }

    public static void main(String[] args) {
        SpringApplication.run(RelationApplication.class, args);
    }

}
