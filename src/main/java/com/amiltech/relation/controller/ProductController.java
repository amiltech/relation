package com.amiltech.relation.controller;

import com.amiltech.relation.dto.product.request.ProductCreateRequestDTO;
import com.amiltech.relation.entity.Product;
import com.amiltech.relation.genericspec.ProductSpecification;
import com.amiltech.relation.genericspec.SearchCriteria;
import com.amiltech.relation.repository.ProductRepository;
import com.amiltech.relation.service.ProductService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.List;


@RestController
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class ProductController {
    ProductService productCreateService;
    ProductRepository productRepository;

    @PostMapping("/create")
    public void create(@RequestBody ProductCreateRequestDTO dto) {
        productCreateService.createProduct(dto);
    }


    @PostMapping("/search")
    public Collection<Product> search(@RequestBody List<SearchCriteria> list){
        return productRepository.findAll(new ProductSpecification(list));
    }
}
