package com.amiltech.relation.controller;

import com.amiltech.relation.dto.product.request.ProductCreateRequestDTO;
import com.amiltech.relation.dto.shoppingCards.request.ShoppingCardAddProductRequestDTO;
import com.amiltech.relation.dto.shoppingCards.request.ShoppingCardCreateRequestDTO;
import com.amiltech.relation.entity.ShoppingCards;
import com.amiltech.relation.service.ShoppingCardsService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.*;


@RestController
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class ShoppingCardsController {
    ShoppingCardsService shoppingCardsService;

    @PostMapping("/shopping-carts")
    public ShoppingCards createShoppingCart(@RequestBody ShoppingCardCreateRequestDTO dto) {
        return shoppingCardsService.createShoppingCard(dto);
    }

    @PostMapping("/shopping-carts/{id}/product/add")
    public void addProductToCart(@PathVariable Integer id, @RequestBody ShoppingCardAddProductRequestDTO dto) {
        shoppingCardsService.addProductToCart(id,dto);
    }
    @DeleteMapping("/shopping-carts/{id}/product/remove")
    public void removeProductFromCart(@PathVariable Integer id, @RequestBody ShoppingCardAddProductRequestDTO dto) {
        shoppingCardsService.removeProductFromCart(id,dto);
    }
    @GetMapping("/shopping-carts/{id}")
    public ShoppingCards getShoppingCartById(@PathVariable Integer id) {
        return shoppingCardsService.findShoppingCardById(id);
    }

}
