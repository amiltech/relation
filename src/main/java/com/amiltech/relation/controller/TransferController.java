package com.amiltech.relation.controller;

import com.amiltech.relation.entity.ShoppingCards;
import com.amiltech.relation.repository.ShoppingCardsRepository;
import com.amiltech.relation.service.impl.TransferService;
import jakarta.transaction.Transactional;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class TransferController {

    TransferService transferService;
    ShoppingCardsRepository shoppingCardsRepository;
    List<ShoppingCards> list = new ArrayList<>();

//    @GetMapping("/1")
//    public void transfer1() {
//        transferService.transfer(1,2,30);
//    }
//
//
//    @GetMapping("/2")
//    public void transsfer2() {
//        transferService.transfer(1,2,40);
//    }

    @GetMapping("/hello")
    public List<ShoppingCards> sayHello() {
        List<ShoppingCards> all = shoppingCardsRepository.findAll();
        list.addAll(all);
        return all;
    }

    @GetMapping("/1")
    @Transactional
    public ShoppingCards sayHello1() throws Exception {
        Thread.sleep(300);
        return shoppingCardsRepository.findById(1).get();
    }

    @GetMapping("/2")
    public ShoppingCards sayHello2() {
        return shoppingCardsRepository.findById(2).get();
    }
}
