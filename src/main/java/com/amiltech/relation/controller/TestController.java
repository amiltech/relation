package com.amiltech.relation.controller;

import com.amiltech.relation.dto.student.StudentCreateDto;
import com.amiltech.relation.service.impl.StudentService;
import com.amiltech.relation.service.impl.TranslateService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController()
@RequestMapping("/test")
@Validated
public class TestController {

    @Autowired
    private TranslateService translateService;

    @Autowired
    private StudentService studentService;


    @GetMapping("/translate")
    public ResponseEntity<String> translate(@RequestHeader("Accept-Language") String language, @RequestParam String[] name) {
        return ResponseEntity.ok(translateService.translate("welcome_message",language,name));
    }
    @GetMapping("/exception")
    public void sendError(@RequestHeader("Accept-Language") String language, @RequestParam String[] name) {
        studentService.test(name);
    }

    @GetMapping ("/aa")
    public ResponseEntity<String> testValidation(@Valid @RequestBody StudentCreateDto dto) {
        return ResponseEntity.ok("Validation Passed");
    }

    @GetMapping ("/a")
    public ResponseEntity<String> a() {
        return ResponseEntity.ok("Validation Passed");
    }


    @GetMapping ("/b")
    public ResponseEntity<String> b() {
        System.err.println("Validation Passed");
        return ResponseEntity.ok("Validation Passed");
    }


    @GetMapping ("/c")
    public ResponseEntity<String> c() {
        return ResponseEntity.ok("Validation Passed");
    }
}
