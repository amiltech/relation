package com.amiltech.relation.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "ms-02",url = "http://localhost:9009")
public interface Ms02FeignClient {
    @GetMapping("/hello/ms-02")
    String helloMs02();
}
