package com.amiltech.relation.entity;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@NoArgsConstructor
@Builder
@Getter
@Setter
@AllArgsConstructor
@Entity
@Table(name = "shopping_cards")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)

public class ShoppingCards implements Serializable {

    private static final long serialVersionUID = 1L;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    String name;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "product_shopping_cards",
            joinColumns = @JoinColumn(name = "shopping_cards_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "product_id", referencedColumnName = "id")
    )
    List<Product> products;

    @PostLoad
    public void postLoad() {
        // Assume a 10% discount on the price
        this.products = new ArrayList<>();
    }

}
