package com.amiltech.relation.entity;


import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

//import javax.cache.annotation.CachePut;
import java.io.Serial;
import java.io.Serializable;
import java.sql.Timestamp;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Builder
//@CachePut
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Student implements Serializable {

    @Serial
    private static final long serialVersionUID = 6282149666543503699L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String name;
    Boolean active;
    Timestamp birthDay;
    Integer age;
    String password;

    public Student(Student student) {
        this.id = student.getId();
        this.name = student.getName();
        this.active = student.getActive();
        this.birthDay = student.getBirthDay();
        this.age = student.getAge();
        this.password = student.getPassword();
    }

}
