package com.amiltech.relation.entity;


import com.amiltech.relation.constraints.Role;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.security.core.GrantedAuthority;

import java.io.Serial;
import java.io.Serializable;
import java.sql.Timestamp;


@NoArgsConstructor
@Getter
@Setter
@AllArgsConstructor
@Embeddable
@Builder
public class Authority implements GrantedAuthority {

    @Enumerated(EnumType.STRING)
    Role authority;

    @Override
    public String getAuthority() {
        return authority.name();
    }
}
