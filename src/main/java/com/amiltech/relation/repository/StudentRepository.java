package com.amiltech.relation.repository;

import com.amiltech.relation.entity.Category;
import com.amiltech.relation.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Long> {
}
