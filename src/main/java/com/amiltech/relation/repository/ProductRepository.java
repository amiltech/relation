package com.amiltech.relation.repository;

import com.amiltech.relation.entity.Category;
import com.amiltech.relation.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ProductRepository extends JpaRepository<Product, Integer> , JpaSpecificationExecutor<Product> {
}
