package com.amiltech.relation.repository;

import com.amiltech.relation.entity.Category;
import com.amiltech.relation.entity.ProductDetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductDetailsRepository extends JpaRepository<ProductDetails, Long> {
}
