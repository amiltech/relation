package com.amiltech.relation.repository;

import com.amiltech.relation.entity.Category;
import com.amiltech.relation.entity.ShoppingCards;
import jakarta.persistence.QueryHint;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.QueryHints;

import java.util.Optional;

public interface ShoppingCardsRepository extends JpaRepository<ShoppingCards, Integer> {


    @Override
    @QueryHints({@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    Optional<ShoppingCards> findById(Integer integer);
}
