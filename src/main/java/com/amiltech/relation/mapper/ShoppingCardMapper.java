package com.amiltech.relation.mapper;


import com.amiltech.relation.dto.shoppingCards.request.ShoppingCardCreateRequestDTO;
import com.amiltech.relation.entity.ShoppingCards;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ShoppingCardMapper {

    ShoppingCards dtoToEntity(ShoppingCardCreateRequestDTO dto);
}
