package com.amiltech.relation.mapper;

import com.amiltech.relation.dto.student.StudentDto;
import com.amiltech.relation.entity.Student;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface StudentMapper {
    StudentDto studentToStudentDto(Student student);
}
