package com.amiltech.relation.dto.student;

import java.sql.Timestamp;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StudentDto {
    Long id;
    String name;
    Boolean active;
    Timestamp birthDay;
    Integer age;
}
