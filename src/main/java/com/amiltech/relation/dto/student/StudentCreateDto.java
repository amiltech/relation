package com.amiltech.relation.dto.student;

import jakarta.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder

public class StudentCreateDto {

    @NotBlank(message = "validation_error_001")
    @Size(min = 2, max = 50, message = "validation_error_002") // Add custom message
    String name;

    @Min(value = 17, message = "validation_error_003")
    @Max(value = 1000, message = "validation_error_004")
    Integer age;

    Boolean active;

    Timestamp birthDay;
}
