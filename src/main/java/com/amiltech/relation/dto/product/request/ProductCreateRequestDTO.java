package com.amiltech.relation.dto.product.request;

import com.amiltech.relation.entity.ProductDetails;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductCreateRequestDTO {
    Long categoryId;
    ProductDetails productDetails;


}
