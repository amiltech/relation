package com.amiltech.relation.dto.shoppingCards.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ShoppingCardRemoveProductRequestDTO {
    Integer productId;
}
