package com.amiltech.relation.exception;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorResponseDTO {
    private int status;
    private String message;
    private String details;
    private ErrorCode code;
    private String path;
    private OffsetDateTime timestamp;
    @JsonProperty("requested_lang")
    private String requestedLang;
    @Builder.Default
    private Map<String, Object> data = new HashMap<>();
}
