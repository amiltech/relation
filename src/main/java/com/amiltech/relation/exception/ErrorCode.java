package com.amiltech.relation.exception;


import lombok.Getter;

@Getter
public enum ErrorCode {

    STUDENT_NOT_FOUND_0001("0001"),
    ENTITY_NOT_FOUND_0001("0002");

    private final String code;

    ErrorCode(String code){
        this.code = code;

    }
}
