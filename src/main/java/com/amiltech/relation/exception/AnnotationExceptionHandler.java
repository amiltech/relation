package com.amiltech.relation.exception;

import com.amiltech.relation.service.impl.TranslateService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
@RequiredArgsConstructor
public class AnnotationExceptionHandler {

    private final TranslateService translateService;

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ErrorResponseDTO> NotFoundException(NotFoundException e, WebRequest request) {
        String language = request.getHeader("Accept-Language");
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(ErrorResponseDTO.builder()
                        .status(HttpStatus.NOT_FOUND.value())
                        .message(translateService.translate(e.getCode().getCode(),language,e.getArgs()))
                        .details(translateService.translate(e.getCode().getCode().concat("_details"),language,e.getArgs()))
                        .code(e.getCode())
                        .requestedLang(language)
                        .path(((ServletWebRequest) request).getRequest().getRequestURI())
                        .build());
    }


    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorResponseDTO> methodArgumentNotValidException(MethodArgumentNotValidException e, WebRequest request) {
        String language = request.getHeader("Accept-Language");

        List<ConstrainsViolationError> collect = e.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(error -> new ConstrainsViolationError(error.getField(), error.getDefaultMessage())) // Keep the default message as it is
                .collect(Collectors.toList());

        ErrorResponseDTO errorResponseDTO = ErrorResponseDTO.builder()
                .status(HttpStatus.BAD_REQUEST.value()) // Changed to BAD_REQUEST
                .message(e.getMessage()) // Generic message
                .details(e.getMessage()) // Generic details message
                .requestedLang(language)
                .path(((ServletWebRequest) request).getRequest().getRequestURI())
                .build();

        // Populate the error data with translations using the keys from the annotations
        collect.forEach(val -> {
            // Here, we directly use the default message from the validation annotation, which is the key
            String translatedMessage = translateService.translate(val.getProperty(), language);
            errorResponseDTO.getData().put(val.getMessage(), translatedMessage); // Use the property as the key for the response
        });

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponseDTO);
    }
}
