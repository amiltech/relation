package com.amiltech.relation.exception;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ConstrainsViolationError {
    private String message;
    private String property;
}
