package com.amiltech.relation.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StudentNotFoundException extends NotFoundException {
    public StudentNotFoundException(ErrorCode code, String... args) {
        super(code, args);
    }
}
