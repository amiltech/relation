package com.amiltech.relation.exception;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NotFoundException extends RuntimeException {
    ErrorCode code;
    String[] args;
    public NotFoundException(ErrorCode code,String... args) {
        this.code = code;
        this.args = args;
    }
}
