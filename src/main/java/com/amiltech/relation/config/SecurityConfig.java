package com.amiltech.relation.config;

import com.amiltech.relation.constraints.Role;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@RequiredArgsConstructor

public class SecurityConfig {

    private final AuthRequestFilter authRequestFilter;


    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.cors(AbstractHttpConfigurer::disable);
        http.csrf(AbstractHttpConfigurer::disable);
//        http.authorizeHttpRequests(auth -> auth.requestMatchers(HttpMethod.GET, "/a").permitAll());
        http.authorizeHttpRequests(auth -> auth.requestMatchers(HttpMethod.GET, "/user/**").permitAll());
        http.authorizeHttpRequests(auth -> auth.requestMatchers(HttpMethod.POST, "/user/**").permitAll());
        http.authorizeHttpRequests(auth -> auth.requestMatchers(HttpMethod.GET, "/test/b").hasAnyRole(Role.USER.name(), Role.ADMIN.name()));
        http.authorizeHttpRequests(auth -> auth.requestMatchers(HttpMethod.GET, "test/c").hasRole(Role.ADMIN.name()));
        http.authorizeHttpRequests(auth -> auth.anyRequest().authenticated());
        http.addFilterBefore(authRequestFilter, UsernamePasswordAuthenticationFilter.class);

        return http.build();
    }


//    @Bean
//    public UserDetailsService userDetailsService() {
//        UserDetails user = User.withDefaultPasswordEncoder()
//                .username("user")
//                .password("123")
//                .roles("USER")
//                .build();
//
//
//        UserDetails admin = User.withDefaultPasswordEncoder()
//                .username("admin")
//                .password("123")
//                .roles("ADMIN")
//                .build();
//
//
//        return new InMemoryUserDetailsManager(user, admin);
//    }


    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
