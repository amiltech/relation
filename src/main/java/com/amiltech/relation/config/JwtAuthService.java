package com.amiltech.relation.config;


import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class JwtAuthService implements AuthService {
    private final BaseJwtService baseJwtService;

    @Override
    public Optional<Authentication> getAuthentication(HttpServletRequest httpServletRequest) {
        String header = httpServletRequest.getHeader("Authorization");
        if (header != null && header.startsWith("Bearer ")) {
            System.err.println("128");
            String token = header.substring(7);
            System.err.println("256");
            Jws<Claims> claimsJws = baseJwtService.parseToken(token);
            System.err.println("512");
            Authentication authentication = getAuthentication(claimsJws.getPayload());
            System.err.println("1024");
            return Optional.of(authentication);
        }
        return Optional.empty();
    }


    private Authentication getAuthentication(Claims claims) {
        List<String> roles = (List) claims.get("authority");
        List<GrantedAuthority> authorityList = roles
                .stream()
                .map(a->new SimpleGrantedAuthority("ROLE_"+a))
                .collect(Collectors.toList());

        JwtCredentials credentials = new ModelMapper().map(claims, JwtCredentials.class);

        return new UsernamePasswordAuthenticationToken(credentials, null, authorityList);
    }
}

